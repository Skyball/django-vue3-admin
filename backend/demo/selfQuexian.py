# -*- coding: utf-8 -*-

"""
文件说明：这里可以写一段简短的描述文件的作用和功能。
作者：你的名字
日期：当前日期
"""

# 导入所需的模块

# 定义全局变量

# 定义类和函数

class A:
    def __init__(self):
        # 检测出
        return None

class B:
    def __init__(self):
        # 未检测
        self.inited = True

def foo():
    # 未检测
    return 5

# 执行代码

if __name__ == "__main__":
    # 在这里写入测试代码或者调用函数来验证代码的正确性
    foo()